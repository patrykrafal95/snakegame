﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snake
{
    public partial class Form1 : Form
    {
        private bool _isStarted;
        private Model.Snake _snake;
        private Model.Fruit _fruit;
        public Form1()
        {
            InitializeComponent();
            this._isStarted = false;
            this.timer1.Enabled = true;
            this.pauzaToolStripMenuItem.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_isStarted)
            {
                pole_gry.CreateGraphics().Clear(Color.Black);
                _snake.MoveSnake();
                DrawSnake(pole_gry.CreateGraphics(), new SolidBrush(Color.Aqua));
                DrawFruit(pole_gry.CreateGraphics(), new SolidBrush(Color.Red));
                if (_fruit.RandomNewFruit(_snake.X[0], _snake.Y[0]))
                {
                    _snake.AddFruit();
                }
                if (!_snake.SnakeAlive())
                {
                    _isStarted = false;
                }
            }
            else
            {
                FontFamily fontFamily = new FontFamily("Arial");
                Font font = new Font(fontFamily, 15);
                Brush b = new SolidBrush(Color.Aqua);
                this.pole_gry.CreateGraphics().DrawString("Start ", font, b, 90, 100);
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._isStarted = true;
            _snake = new Model.Snake(pole_gry.Width, pole_gry.Height);
            _fruit = new Model.Fruit(_snake.Segemnt);
            this.pauzaToolStripMenuItem.Enabled = true;
        }

        #region draw snake
        public void DrawSnake(Graphics g, Brush b)
        {
            g.FillRectangle(new SolidBrush(Color.Green), _snake.X[0], _snake.Y[0], _snake.Segemnt, _snake.Segemnt);
            for (int i = 1; i < _snake.Segmenty; i++)
            {
                g.FillRectangle(b, _snake.X[i], _snake.Y[i], _snake.Segemnt, _snake.Segemnt);
            }
        }
        #endregion

        #region draw fruit
        public void DrawFruit(Graphics g, Brush b)
        {
            g.FillRectangle(b, _fruit.X, _fruit.Y, _fruit.Segment, _fruit.Segment);
        }
        #endregion

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up) _snake.Move = Model.Move.GORA;
            if (e.KeyCode == Keys.Down) _snake.Move = Model.Move.DOL;
            if (e.KeyCode == Keys.Right) _snake.Move = Model.Move.PRAWO;
            if (e.KeyCode == Keys.Left) _snake.Move = Model.Move.LEWO;
        }

        private void pauzaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._isStarted)
            {
                this._isStarted = false;
                this.pauzaToolStripMenuItem.Text = "Wznów";
                pole_gry.CreateGraphics().Clear(Color.Black);
            }
            else
            {
                this._isStarted = true;
                this.pauzaToolStripMenuItem.Text = "Pauza";
            }
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._isStarted)
            {
                _snake = new Model.Snake(pole_gry.Width, pole_gry.Height);
                _fruit = new Model.Fruit(_snake.Segemnt);
            }
        }

        private void wolniejToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.timer1.Interval += 10;
        }

        private void szybciejToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.timer1.Interval > 10)
            {
                this.timer1.Interval -= 10;
            }
        }
    }
}
