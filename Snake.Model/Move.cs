﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snake.Model
{
    public enum Move
    {
        START,
        PRAWO,
        LEWO,
        GORA,
        DOL
    }
}
